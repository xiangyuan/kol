package util

import (
	"fmt"
	"testing"
)

type User struct {
	UserName string `json:"username"`
}

type Student struct {
	UserName string `json:"username"`
	Password string `json:"password"`
}

func TestByteToObj(t *testing.T) {
	var user User
	ByteToObj([]byte(`{"username":"123"}`), &user)
	fmt.Println(user)
}

func TestCopyProperties(t *testing.T) {
	var user = User{"张三"}
	var student Student
	CopyProperties(&user, &student)
	fmt.Println(student)
}
