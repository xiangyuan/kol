package util

import (
	"encoding/json"
)

func ByteToObj(b []byte, o interface{}) {
	json.Unmarshal(b, o)
}

func ObjToObj(src interface{}, target interface{}) {
	bytes, _ := json.Marshal(src)
	ByteToObj(bytes, target)
}

func CopyProperties(src interface{}, target interface{}) {
	bytes, _ := json.Marshal(src)
	json.Unmarshal(bytes, target)
}
