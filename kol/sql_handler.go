package kol

import (
	"bytes"
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

/**
 * 修改对象时的包装对象
 */
type UpdateWrap struct {
	UpdateField interface{}
	WhereField  interface{}
}

func tableName(v reflect.Value) string {
	if method := v.MethodByName("TableName"); method.String() == "<invalid Value>" {
		return "t_" + conversField(v.Elem().Type().Name())
	} else {
		return method.Call([]reflect.Value{})[0].String()
	}
}

/**
 * 转换字段 UserName -> user_name
 */
func conversField(field string) string {
	for _, char := range field {
		if 65 <= char && char <= 90 {
			field = strings.Replace(field, string(char), "_"+string(char+32), -1)
		}
	}
	if strings.HasPrefix(field, "_") {
		return field[1:]
	}
	return field
}

/**
 * 解析struct字段
 */
func analysisStructField(t reflect.Type, v reflect.Value) ([]string, []string) {
	var fields []string
	var values []string
	if t.Kind() == reflect.Ptr {
		for i := 0; i < t.Elem().NumField(); i++ {
			switch v.Elem().Field(i).Kind() {
			case reflect.Int:
				if v.Elem().Field(i).Int() != 0 {
					column := t.Elem().Field(i).Tag.Get("column")
					if len(column) == 0 {
						fields = append(fields, conversField(t.Elem().Field(i).Name))
					} else {
						fields = append(fields, column)
					}
					values = append(values, strconv.FormatInt(v.Elem().Field(i).Int(), 10))
				}
			case reflect.String:
				if len(v.Elem().Field(i).String()) != 0 {
					column := t.Elem().Field(i).Tag.Get("column")
					if len(column) == 0 {
						fields = append(fields, conversField(t.Elem().Field(i).Name))
					} else {
						fields = append(fields, column)
					}
					values = append(values, v.Elem().Field(i).String())
				}
			}
		}
	}
	return fields, values
}

/**
 * fetch select sql
 */
func StructToSelectSql(o interface{}) string {
	t := reflect.TypeOf(o)
	v := reflect.ValueOf(o)
	tableName := tableName(v)
	fields, values := analysisStructField(t, v)
	if len(fields) <= 0 {
		return fmt.Sprintf("select * from %s", tableName)
	}
	var value bytes.Buffer
	for i, f := range fields {
		value.WriteString("`")
		value.WriteString(f)
		value.WriteString("`=")
		_, e := strconv.Atoi(values[i])
		if e == nil {
			value.WriteString(values[i])
			value.WriteString(" and ")
		} else {
			value.WriteString("'")
			value.WriteString(values[i])
			value.WriteString("' and ")
		}
	}
	valueStr := value.String()
	return fmt.Sprintf("select * from %s where %s", tableName, valueStr[:len(valueStr)-len(" and ")])
}

/**
 * fetch insert sql
 */
func StructToInsertSql(o interface{}) string {
	t := reflect.TypeOf(o)
	v := reflect.ValueOf(o)
	tableName := tableName(v)
	fields, values := analysisStructField(t, v)
	var field bytes.Buffer
	for _, f := range fields {
		field.WriteString("`")
		field.WriteString(f)
		field.WriteString("`,")
	}
	var value bytes.Buffer
	for _, f := range values {
		value.WriteString("'")
		value.WriteString(f)
		value.WriteString("',")
	}
	fieldStr := field.String()
	valueStr := value.String()
	return fmt.Sprintf("insert into %s (%s) values (%s)", tableName, fieldStr[0:len(fieldStr)-1], valueStr[0:len(valueStr)-1])
}

/**
 * fetch delete sql
 */
func StructToDeleteSql(o interface{}) string {
	t := reflect.TypeOf(o)
	v := reflect.ValueOf(o)
	tableName := tableName(v)
	fields, values := analysisStructField(t, v)

	var value bytes.Buffer
	for i, f := range fields {
		value.WriteString("`")
		value.WriteString(f)
		value.WriteString("`=")
		value.WriteString("'")
		value.WriteString(values[i])
		value.WriteString("' and ")
	}
	valueStr := value.String()
	if len(valueStr) == 0 {
		panic("error deleting statement")
	}
	if strings.HasSuffix(valueStr, " and ") {
		valueStr = valueStr[:len(valueStr)-len(" and ")]
	}
	return fmt.Sprintf("delete from %s where %s", tableName, valueStr)
}

/**
 * fetch update sql
 */
func StructToUpdateSql(u *UpdateWrap) string {
	ut := reflect.TypeOf(u.UpdateField)
	uv := reflect.ValueOf(u.UpdateField)

	tableName := tableName(uv)

	fields, values := analysisStructField(ut, uv)
	var updateSql bytes.Buffer
	for i, f := range fields {
		updateSql.WriteString("`")
		updateSql.WriteString(f)
		updateSql.WriteString("`")
		updateSql.WriteString("=")
		updateSql.WriteString("'")
		updateSql.WriteString(values[i])
		updateSql.WriteString("'")
		updateSql.WriteString(",")
	}

	wt := reflect.TypeOf(u.WhereField)
	wv := reflect.ValueOf(u.WhereField)
	fields, values = analysisStructField(wt, wv)
	var whereSql bytes.Buffer
	for i, f := range fields {
		whereSql.WriteString("`")
		whereSql.WriteString(f)
		whereSql.WriteString("`")
		whereSql.WriteString("=")
		whereSql.WriteString("'")
		whereSql.WriteString(values[i])
		whereSql.WriteString("'")
		whereSql.WriteString(" and ")
	}
	fieldStr := updateSql.String()
	valueStr := whereSql.String()
	return fmt.Sprintf("update %s set %s where %s", tableName, fieldStr[0:len(fieldStr)-1], valueStr[:len(valueStr)-len(" and ")])
}
