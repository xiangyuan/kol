package kol

import (
	"fmt"
	"testing"
)

type User struct {
	Id       int    `column:"id"`
	UserName string `column:"username"`
	Password string `column:"password"`
}

var dbUrl = "root:123456@tcp(192.168.1.170:3306)/db_test?charset=utf8"

func TestQuery(t *testing.T) {
	Open("mysql", dbUrl)
	var users []User
	QueryStruct(&User{}, "order by id desc", &users)
	fmt.Println(users)
}

func TestQueryPage(t *testing.T) {
	Open("mysql", dbUrl)
	page := NewPage(1, 3)
	var users []User
	QueryStructPage(&User{}, "password='123456'", page, &users)
	fmt.Println(users)
}

func TestInsertStruct(t *testing.T) {
	Open("mysql", dbUrl)
	u := new(User)
	u.UserName = "哈哈"
	u.Password = "562389"
	i, i2, e := InsertStruct(u)
	fmt.Println(i, i2, e)
}

func TestDeleteStruct(t *testing.T) {
	Open("mysql", dbUrl)
	u := new(User)
	u.Id = 3
	i, i2, e := DeleteStruct(u)
	fmt.Println(i, i2, e)
}

func TestUpdateStruct(t *testing.T) {
	Open("mysql", dbUrl)
	u := UpdateWrap{
		&User{UserName: "赵五"},
		&User{Password: "568923"},
	}
	i, i2, e := UpdateStruct(&u)
	fmt.Println(i, i2, e)
}

func TestTx(t *testing.T) {
	Open("mysql", dbUrl)
	tx := new(Tx)
	tx.Begin()
	i, i2, e := tx.InsertStruct(&User{UserName: "王在中", Password: "wzz"})
	if e != nil {
		tx.Rollback()
		return
	}
	fmt.Println(i, i2, e)
	i, i2, e = tx.InsertStruct(&User{UserName: "赵国天", Password: "zhaoguotian"})
	if e != nil {
		tx.Rollback()
		return
	}
	fmt.Println(i, i2, e)
	tx.Commit()
}
