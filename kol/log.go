package kol

import (
	"github.com/lestrrat/go-file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
	"os"
	"time"
)

var Log *logrus.Entry

func init() {

	logPath := "../logs"
	_, e := os.Stat(logPath)
	if os.IsNotExist(e) {
		os.MkdirAll(logPath, os.ModePerm)
	}

	log := logrus.New()
	log.Formatter = &logrus.TextFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
		ForceColors:     true,
		FullTimestamp:   true,
	}
	log.SetLevel(logrus.DebugLevel)

	debug, _ := rotatelogs.New(
		"../logs/debug.%Y%m%d",
		//rotatelogs.WithLinkName(logPath), // 生成软链，指向最新日志文件
		rotatelogs.WithMaxAge(time.Hour*24),       // 文件最大保存时间
		rotatelogs.WithRotationTime(time.Hour*24), // 日志切割时间间隔
	)
	info, _ := rotatelogs.New(
		"../logs/info.%Y%m%d",
		//rotatelogs.WithLinkName(logPath),
		rotatelogs.WithMaxAge(time.Hour*24),
		rotatelogs.WithRotationTime(time.Hour*24),
	)
	warn, _ := rotatelogs.New(
		"../logs/warn.%Y%m%d",
		//rotatelogs.WithLinkName(logPath),
		rotatelogs.WithMaxAge(time.Hour*24),
		rotatelogs.WithRotationTime(time.Hour*24),
	)
	error, _ := rotatelogs.New(
		"../logs/error.%Y%m%d",
		//rotatelogs.WithLinkName(logPath),
		rotatelogs.WithMaxAge(time.Hour*24*365),
		rotatelogs.WithRotationTime(time.Hour*24),
	)
	lfHook := lfshook.NewHook(lfshook.WriterMap{
		logrus.DebugLevel: debug,
		logrus.InfoLevel:  info,
		logrus.WarnLevel:  warn,
		logrus.ErrorLevel: error,
		logrus.FatalLevel: error,
		logrus.PanicLevel: error,
	}, &logrus.JSONFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
	})
	log.AddHook(lfHook)
	//设置全局字段
	Log = log.WithFields(logrus.Fields{})
}
