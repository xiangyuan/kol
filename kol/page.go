package kol

/**
 * 分页对象
 */
type Page struct {
	page     int64 //当前页码，从1开始
	pageSize int64 //页面大小
	total    int64 //总条数
	pages    int64 //总页数
}

func NewPage(page, pageSize int64) *Page {
	p := new(Page)
	p.SetPage(page)
	p.SetPageSize(pageSize)
	return p
}

func (p *Page) SetPage(page int64) {
	if page <= 0 {
		p.page = 1
	} else {
		p.page = page
	}
}

func (p *Page) GetPage() int64 {
	return p.page
}

func (p *Page) SetPageSize(pageSize int64) {
	if pageSize <= 0 {
		p.pageSize = 1
	} else {
		p.pageSize = pageSize
	}
}

func (p *Page) GetPageSize() int64 {
	return p.pageSize
}

func (p *Page) SetTotal(total int64) {
	p.total = total
	t := p.total % p.pageSize
	if t > 0 {
		p.pages = p.total/p.pageSize + 1
	} else {
		p.pages = p.total / p.pageSize
	}
}

func (p *Page) GetTotal() int64 {
	return p.total
}

func (p *Page) GetPages() int64 {
	return p.pages
}
