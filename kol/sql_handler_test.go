package kol

import (
	"fmt"
	"testing"
)

func (u *User) TableName() string {
	return "t_user"
}

func TestSelectSql(t *testing.T) {
	user := User{}
	user.Id = 23
	sql := StructToSelectSql(&user)
	fmt.Println(sql)
}

func TestInsertSql(t *testing.T) {
	user := User{}
	user.UserName = "张三"
	user.Password = "123456"
	sql := StructToInsertSql(&user)
	fmt.Println(sql)
}

func TestDeleteSql(t *testing.T) {
	user := User{}
	user.UserName = "张三"
	user.Password = "123456"
	sql := StructToDeleteSql(&user)
	fmt.Println(sql)
}

func TestUpdateSql(t *testing.T) {
	user := User{}
	user.UserName = "张三"
	user.Password = "123456"
	updateWrap := new(UpdateWrap)
	updateWrap.UpdateField = &User{UserName: "赵四"}
	updateWrap.WhereField = &User{Id: 23}
	sql := StructToUpdateSql(updateWrap)
	fmt.Println(sql)
}
