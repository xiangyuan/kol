package kol

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"kol/util"
	"strconv"
	"strings"
)

var db *sql.DB

func Open(driverName, dataSourceName string) error {
	var err error
	db, err = sql.Open(driverName, dataSourceName)
	if err != nil {
		return err
	}
	return nil
}

/**
 * 查询sql 返回 []map[string]interface{}, error
 */
func QuerySql(sql string) ([]map[string]interface{}, error) {
	Log.Debug(sql)
	rows, e := db.Query(sql)
	if e != nil {
		return nil, e
	}
	return rowsToMap(rows), nil
}

/**
 * condition : 条件
 * sqlExpansion : sql语句扩展，比如（最一个and不用加）
 * 要查 时间范围  startTime < t and t < endTime 或排序 order by createTime desc
 * obj : 从数据取出数据装入到 obj里
 */
func QueryStruct(condition interface{}, sqlExpansion string, obj interface{}) error {
	sql := StructToSelectSql(condition)
	if len(sqlExpansion) > 0 {
		if strings.HasPrefix(sqlExpansion, "order by") {
			sql = sql + " " + sqlExpansion
		} else {
			if strings.Contains(sql, " where ") {
				sql = sql + " and " + sqlExpansion
			} else {
				sql = sql + " where " + sqlExpansion
			}
		}
	}
	Log.Debug(sql)
	rows, e := db.Query(sql)
	if e != nil {
		return e
	}
	table := rowsToMap(rows)
	util.ObjToObj(table, obj)
	return nil
}

/**
 * 分页查询
 * condition : 条件
 * sqlExpansion : sql语句扩展，比如（最前一个and不用加）
 * 要查 时间范围  startTime < t and t < endTime 或排序 order by createTime desc
 * page : 分页
 * obj : 从数据取出数据装入到 obj里
 */
func QueryStructPage(condition interface{}, sqlExpansion string, page *Page, obj interface{}) error {
	sql := StructToSelectSql(condition)
	if len(sqlExpansion) > 0 {
		if strings.HasPrefix(sqlExpansion, "order by") {
			sql = sql + " " + sqlExpansion
		} else {
			if strings.Contains(sql, " where ") {
				sql = sql + " and " + sqlExpansion
			} else {
				sql = sql + " where " + sqlExpansion
			}
		}
	}
	count, _ := QuerySql(strings.Replace(sql, "*", "count(1) as count", 1))
	total, _ := strconv.ParseInt(count[0]["count"].(string), 10, 0)
	page.SetTotal(total)
	if total <= 0 {
		return nil
	}
	sql = sql + " limit " + strconv.FormatInt((page.GetPage()-1)*page.GetPageSize(), 10) + ", " + strconv.FormatInt(page.GetPageSize(), 10)
	Log.Debug(sql)
	rows, e := db.Query(sql)
	if e != nil {
		return e
	}
	table := rowsToMap(rows)
	util.ObjToObj(table, obj)
	return nil
}

func InsertSql(sql string) (int64, int64, error) {
	return exec(sql)
}

func InsertStruct(o interface{}) (int64, int64, error) {
	sql := StructToInsertSql(o)
	return exec(sql)
}

func DeleteSql(sql string) (int64, int64, error) {
	return exec(sql)
}

func DeleteStruct(o interface{}) (int64, int64, error) {
	sql := StructToDeleteSql(o)
	return exec(sql)
}

func UpdateSql(sql string) (int64, int64, error) {
	return exec(sql)
}

func UpdateStruct(u *UpdateWrap) (int64, int64, error) {
	sql := StructToUpdateSql(u)
	return exec(sql)
}

func exec(sql string) (int64, int64, error) {
	Log.Debug(sql)
	result, e := db.Exec(sql)
	if e != nil {
		return 0, 0, e
	}
	lastInsertId, _ := result.LastInsertId()
	rowsAffected, _ := result.RowsAffected()
	return lastInsertId, rowsAffected, nil
}

/**
 * rows 转 map[string]interface{}
 */
func rowsToMap(rows *sql.Rows) []map[string]interface{} {
	defer rows.Close()
	var table []map[string]interface{}
	cols, _ := rows.Columns()
	for rows.Next() {
		row := make(map[string]interface{}, len(cols))
		p := make([]interface{}, len(cols))
		for index, value := range cols {
			var t interface{}
			row[value] = &t
			p[index] = &t
		}
		rows.Scan(p...)
		table = append(table, row)
	}
	for _, r := range table {
		for k, _ := range r {
			r[k] = fmt.Sprintf("%s", *r[k].(*interface{}))
		}
	}
	return table
}

type Tx struct {
	t *sql.Tx
}

func (tx *Tx) Begin() {
	tx.t, _ = db.Begin()
}

func (tx *Tx) Rollback() {
	tx.t.Rollback()
}

func (tx *Tx) Commit() {
	tx.t.Commit()
}

/**
 * 查询sql 返回 []map[string]interface{}, error
 */
func (tx *Tx) QuerySql(sql string) ([]map[string]interface{}, error) {
	Log.Debug(sql)
	rows, e := tx.t.Query(sql)
	if e != nil {
		return nil, e
	}
	return rowsToMap(rows), nil
}

/**
 * condition : 条件
 * sqlExpansion : sql语句扩展，比如（最一个and不用加）
 * 要查 时间范围  startTime < t and t < endTime 或排序 order by createTime desc
 * obj : 从数据取出数据装入到 obj里
 */
func (tx *Tx) QueryStruct(condition interface{}, sqlExpansion string, obj interface{}) error {
	sql := StructToSelectSql(condition)
	if len(sqlExpansion) > 0 {
		if strings.HasPrefix(sqlExpansion, "order by") {
			sql = sql + " " + sqlExpansion
		} else {
			if strings.Contains(sql, " where ") {
				sql = sql + " and " + sqlExpansion
			} else {
				sql = sql + " where " + sqlExpansion
			}
		}
	}
	Log.Debug(sql)
	rows, e := db.Query(sql)
	if e != nil {
		return e
	}
	table := rowsToMap(rows)
	util.ObjToObj(table, obj)
	return nil
}

/**
 * 分页查询
 * condition : 条件
 * sqlExpansion : sql语句扩展，比如（最前一个and不用加）
 * 要查 时间范围  startTime < t and t < endTime 或排序 order by createTime desc
 * page : 分页
 * obj : 从数据取出数据装入到 obj里
 */
func (tx *Tx) QueryStructPage(condition interface{}, sqlExpansion string, page *Page, obj interface{}) error {
	sql := StructToSelectSql(condition)
	if len(sqlExpansion) > 0 {
		if strings.HasPrefix(sqlExpansion, "order by") {
			sql = sql + " " + sqlExpansion
		} else {
			if strings.Contains(sql, " where ") {
				sql = sql + " and " + sqlExpansion
			} else {
				sql = sql + " where " + sqlExpansion
			}
		}
	}
	count, _ := tx.QuerySql(strings.Replace(sql, "*", "count(1) as count", 1))
	total, _ := strconv.ParseInt(count[0]["count"].(string), 10, 0)
	page.SetTotal(total)
	if total <= 0 {
		return nil
	}
	sql = sql + " limit " + strconv.FormatInt((page.GetPage()-1)*page.GetPageSize(), 10) + ", " + strconv.FormatInt(page.GetPageSize(), 10)
	Log.Debug(sql)
	rows, e := tx.t.Query(sql)
	if e != nil {
		return e
	}
	table := rowsToMap(rows)
	util.ObjToObj(table, obj)
	return nil
}

func (tx *Tx) InsertSql(sql string) (int64, int64, error) {
	return tx.exec(sql)
}

func (tx *Tx) InsertStruct(o interface{}) (int64, int64, error) {
	sql := StructToInsertSql(o)
	return tx.exec(sql)
}

func (tx *Tx) DeleteSql(sql string) (int64, int64, error) {
	return tx.exec(sql)
}

func (tx *Tx) DeleteStruct(o interface{}) (int64, int64, error) {
	sql := StructToDeleteSql(o)
	return tx.exec(sql)
}

func (tx *Tx) UpdateSql(sql string) (int64, int64, error) {
	return tx.exec(sql)
}

func (tx *Tx) UpdateStruct(u *UpdateWrap) (int64, int64, error) {
	sql := StructToUpdateSql(u)
	return tx.exec(sql)
}

func (tx *Tx) exec(sql string) (int64, int64, error) {
	Log.Debug(sql)
	result, e := tx.t.Exec(sql)
	if e != nil {
		return 0, 0, e
	}
	lastInsertId, _ := result.LastInsertId()
	rowsAffected, _ := result.RowsAffected()
	return lastInsertId, rowsAffected, nil
}
